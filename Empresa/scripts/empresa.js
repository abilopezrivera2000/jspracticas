function addElementHTML(p, e, c) {
    var parent = document.querySelector(p);
    var tag = document.createElement(e);
    tag.innerHTML = c;
    parent.appendChild(tag);
}



//Creación objeto, funcion constructora
function Persona(nombre, sexo, edad) {

    //se crean objetos de prototipos
    let persona = Object.create(ConstructorPersona);
    persona.nombre = nombre;
    persona.sexo = sexo;
    persona.edad = edad;

    return persona;
}


let ConstructorPersona = {
    info: function() {
        return `${this.nombre}, tiene ${this.edad} y es de sexo ${this.sexo}`;
    }
}


function Gerente(nombre, sexo, edad, depto, personas) {
    let gerente = Persona(nombre, sexo, edad);
    Object.setPrototypeOf(gerente, constructorGerente);
    gerente.depto = depto;
    gerente.personas = personas;
    return gerente;
}


function Empleado(nombre, sexo, edad, depto, posicion) {
    let empleado = Persona(nombre, sexo, edad);
    Object.setPrototypeOf(empleado, constructorEmpleado);
    empleado.depto = depto;
    empleado.posicion = posicion;
    return empleado;
}


let constructorGerente = {
        AdministrarDeptos: function() {
            return `Administra Departamento`;
        },
        RealizarNomina: function() {
            return `Realiza nomina de`;
        }
    }
    //Object.setPrototypeOf(constructorGerente, ConstructorPersona);



let constructorEmpleado = {
        RealizarInventario: function() {
            return `Realiza Inventario`;
        },
        HacerVenta: function() {
            return `Hace Venta`;
        }

    }
    //Object.setPrototypeOf(constructorEmpleado, ConstructorPersona);


let gerente1 = new Gerente("Adrian", "Masculino", 35, "Electronica", 5);
console.log(gerente1);
let gerente2 = new Gerente("María Grcia", "Femenino", 40, "Hogar", 3);
console.log(gerente2);
let gerente3 = new Gerente("Marcela Flores", "Femenino", 42, "Blancos", 2);
console.log(gerente3);


let empleado1 = new Empleado("Sergio Rodriguez", "Masculino", 35, "Electrónica", "Videojuegos");
console.log(empleado1);
let empleado2 = new Empleado("Cásar Flores", "Maculino", 37, "Electrónica", "Sonido");
console.log(empleado2);
let empleado3 = new Empleado("Adriana Ortiz", "Femenino", 30, "Belleza", "Cosméticos");
console.log(empleado3);
let empleado4 = new Empleado("Marcela Cruz", "Femenino", 38, "Belleza", "Perfumería");
console.log(empleado4);
let empleado5 = new Empleado("Rodrigo Chávez", "Maculino", 35, "Hogar", "Electrodomésticos");
console.log(empleado5);
let empleado6 = new Empleado("Cecilia Valdés", "Femenino", 40, "Hogar", "Blancos");
console.log(empleado6);


//document.getElementByID("empleado").innerHTML = `<section>${empleado1}</section>`;
//document.getElementById("ger").innerHTML = `<section>${gerente1}</section>`;
//document.getElementById("ger").innerHTML = `<section>${gerente1.Gerente()}</section>`;

document.getElementById("ger1").innerHTML = `<section>${gerente1.nombre + ", " + gerente1.sexo + " " + gerente1.edad + " años, Encargado de " + gerente1.depto + " y de  " + gerente1.personas + " personas."}</section>`;
document.getElementById("ger2").innerHTML = `<section>${gerente2.nombre + ", " + gerente2.sexo + " " + gerente2.edad + " años, Encargado de " + gerente2.depto + " y de  " + gerente2.personas + " personas"}</section>`;
document.getElementById("ger3").innerHTML = `<section>${gerente3.nombre + ", " + gerente3.sexo + " " + gerente3.edad + " años, Encargado de " + gerente3.depto + " y de  " + gerente3.personas + " personas"}</section>`;

document.getElementById("emp1").innerHTML = `<section>${empleado1.nombre + " " + empleado1.sexo + " " + empleado1.depto + " " + empleado1.posicion}</section>`;
document.getElementById("emp2").innerHTML = `<section>${empleado2.nombre + " " + empleado2.sexo + " " + empleado2.depto + " " + empleado2.posicion}</section>`;
document.getElementById("emp3").innerHTML = `<section>${empleado3.nombre + " " + empleado3.sexo + " " + empleado3.depto + " " + empleado3.posicion}</section>`;
document.getElementById("emp4").innerHTML = `<section>${empleado4.nombre + " " + empleado4.sexo + " " + empleado4.depto + " " + empleado4.posicion}</section>`;
document.getElementById("emp5").innerHTML = `<section>${empleado5.nombre + " " + empleado5.sexo + " " + empleado5.depto + " " + empleado5.posicion}</section>`;
document.getElementById("emp6").innerHTML = `<section>${empleado6.nombre + " " + empleado6.sexo + " " + empleado6.depto + " " + empleado6.posicion}</section>`;